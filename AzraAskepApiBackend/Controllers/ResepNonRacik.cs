using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using AzraAskepApiBackend.Models;
using Newtonsoft.Json.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using AzraAskepApiBackend.Code;

namespace AzraAskepApiBackend.Controllers
{
	[Produces("application/json")]
    [Route("api/ResepNonRacik")]
    public class ResepNonRacikController : Controller
    {
    	private readonly ASKEPContext _context;
        DbSql oDb = new DbSql("Asskep", true);

        StringBuilder sb = new StringBuilder();
        DataSet ds = new DataSet();
        SqlCommand sqlCommand = new SqlCommand();

        StringBuilder sb1 = new StringBuilder();
        DataSet ds1 = new DataSet();
        SqlCommand sqlCommand1 = new SqlCommand();

        String result;

        public ResepNonRacikController(ASKEPContext context)
        {
            _context = context;
        }

        // api awal utk aplikasi dokter igd
        public static DataSet EscapeSpecialChar(DataSet ds)
        {
            int n;
            int x;
            int y;

            for (n = 0; n < ds.Tables.Count; n++)
            {
                DataTable temp = ds.Tables[n];
                for (x = 0; x < temp.Rows.Count; x++)
                {
                    DataRow dr = temp.Rows[x];
                    for (y = 0; y < temp.Columns.Count; y++)
                    {
                        DataColumn dc = temp.Columns[y];

                        if (dr[y].ToString().Contains(@"\"))
                        {
                            dr[y] = dr[y].ToString().Replace(@"\", @"\\");
                        }

                        if (dr[y].ToString().Contains("\""))
                        {
                            dr[y] = dr[y].ToString().Replace("\"", "\\\"");
                        }

                        if (dr[y].ToString().Contains("\\\\"))
                        {
                            dr[y] = dr[y].ToString().Replace("\\\\", "\\\\\\\\");
                        }

                        if (dr[y].ToString().Contains("\\\\\""))
                        {
                            dr[y] = dr[y].ToString().Replace("\\\\\"", "\\\"");
                        }

                        if (dr[y].ToString().Contains("\t"))
                        {
                            dr[y] = dr[y].ToString().Replace("\t", "\\t");
                        }

                        if (dr[y].ToString().Contains("\n"))
                        {
                            dr[y] = dr[y].ToString().Replace("\n", "\\n");
                        }

                        if (dr[y].ToString().Contains("\r"))
                        {
                            dr[y] = dr[y].ToString().Replace("\r", "\\r");
                        }

                        if (dr[y].ToString().Contains("\b"))
                        {
                            dr[y] = dr[y].ToString().Replace("\b", "\\b");
                        }

                        if (dr[y].ToString().Contains("\f"))
                        {
                            dr[y] = dr[y].ToString().Replace("\f", "\\f");
                        }

                        if (dr[y].ToString().Contains("/"))
                        {
                            dr[y] = dr[y].ToString().Replace("/", "\\/");
                        }
                    }
                }
            }
            return ds;
        }

        // api/ResepNonRacik/CekNoSOByNoReg
        [HttpGet("CekNoSO/{no_so}")]
        public IActionResult CekNoSO(string no_so)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXEC igd_CekNoSO @No_SO = '" + no_so.Replace("@", "/") + "'";
                // Console.WriteLine(query);
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        // api/ResepNonRacik/CekNoSOByNoReg
        [HttpGet("CekNoSOByNoReg/{no_reg}")]
        public IActionResult CekNoSOByNoReg(string no_reg)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXEC igd_CekNoSOByNoReg @NoReg = '" + no_reg + "'";
                // Console.WriteLine(query);
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        // api/ResepNonRacik/ObatResep
        [HttpGet("ObatResep/{kd_dokter}")]
        public IActionResult ObatResep(string kd_dokter)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXEC igd_DataObat @Kd_Dokter='" + kd_dokter + "'";
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);

                if (ds.Tables[0].Rows.Count != 0)
                {
                    ds = ResepNonRacikController.EscapeSpecialChar(ds);
                }

                res = DbSql.GetJSONArrayString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JArray.Parse(res));
        }

        // api/ResepNonRacik/GetPaketObatByDok
        [HttpGet("GetPaketObatByDok/{kd_dokter}")]
        public IActionResult GetPaketObatByDok(string kd_dokter)
        {
            string query = "";
            string res = "";

            try
            {
                query = "EXEC igd_GetPaketObatNonRacik @Kd_Dokter = '"+ kd_dokter +"'";
                sb = new StringBuilder();
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONArrayString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JArray.Parse(res));
        }

        // api/ResepNonRacik/PostApSOTemplate_Resep
        [HttpPost("PostApSOTemplate_Resep")]
        public IActionResult PostApSOTemplate_Resep([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXECUTE igd_Insert_ApSOTemplate_Resep @Kd_Dokter = '" + data.kd_dokter + "', @Nm_Template = '" + data.nm_template + "'";
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        // api/ResepNonRacik/PostApSOTemplate_Resep_dt
        [HttpPost("PostApSOTemplate_Resep_dt")]
        public IActionResult PostApSOTemplate_Resep_dt([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                for (int i = 0; i < data.data.Count; i++)
                {
                    sb = new StringBuilder();
                    query = "EXECUTE igd_Insert_ApSOTemplate_Resep_dt @Kd_Dokter = '" + data.kd_dokter + "', @Kd_Brg = '" + data.data[i].Kd_Brg + "', " +
                    "@Aturan_P = '" + data.data[i].Aturan_P + "', @Jml_SO = '" + data.data[i].Jml_SO + "'";
                    // Console.WriteLine(query);
                    sb.AppendFormat(query);
                    sqlCommand = new SqlCommand(sb.ToString());
                    ds = oDb.getDataSetFromSP(sqlCommand);
                    res = DbSql.GetJSONObjectString(ds.Tables[0]);
                }
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        // api/ResepNonRacik/GetObatByPaketObatNonRacik
        [HttpGet("GetObatByPaketObatNonRacik/{kd_dokter}/{kd_temp}")]
        public IActionResult GetObatByPaketObatNonRacik(string kd_dokter, string kd_temp)
        {
            string query = "";
            string res = "";

            try
            {
                query = "EXEC igd_GetPaketObatNonRacikDetail @Kd_Dokter = '"+ kd_dokter +"', @Kd_Template = '"+ kd_temp +"'";
                // Console.WriteLine(query);
                sb = new StringBuilder();
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONArrayString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JArray.Parse(res));
        }

        // api/ResepNonRacik/PostApSOHeader
        [HttpPost("PostApSOHeader")]
        public IActionResult PostApSOHeader([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXECUTE igd_InsertApSOHeader @No_SO = '" + data.No_SO + "', @No_Reg='" + data.No_Reg + "'";
                // Console.WriteLine(query);
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        // api/ResepNonRacik/PostApSONRacik
        [HttpPost("PostApSONRacik")]
        public IActionResult PostApSONRacik([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                // Console.WriteLine(data.data);
                for (int i = 0; i < data.data.Count; i++)
                {
                    sb = new StringBuilder();
                    query = "EXECUTE igd_insertApSONRacik @No_SO = '" + data.No_SO + "', @Kd_Brg='" + data.data[i].Kd_Brg + "', " +
                    "@Aturan_P='" + data.data[i].Aturan_P + "', @Jml_PSO = '" + data.data[i].Jml_SO + "', @Jml_RMW = '" + data.data[i].Jml_RMW + "'";
                     // Console.WriteLine(query);
                     sb.AppendFormat(query);
                     sqlCommand = new SqlCommand(sb.ToString());
                     ds = oDb.getDataSetFromSP(sqlCommand);
                     res = DbSql.GetJSONObjectString(ds.Tables[0]);
                }
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        // api/ResepNonRacik/GetResepObatNonRacik
        [HttpGet("GetResepObatNonRacik/{no_reg}")]
        public IActionResult GetResepObatNonRacik(string no_reg)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXEC igd_GetResepObatNonRacik @No_Reg='" + no_reg + "'";
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONArrayString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JArray.Parse(res));
        }

        // api/ResepNonRacik/DeleteObatNonRacik
        [HttpPost("DeleteObatNonRacik")]
        public IActionResult DeleteObatNonRacik([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXECUTE igd_deleteObatApSONRacik @No_SO = '" + data.no_so + "', @No_Urut = '" + data.no_urut + "'";
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        // api/ResepNonRacik/UpdateApSOHeader
        [HttpPut("UpdateApSOHeader/{no_so}")]
        public IActionResult UpdateApSOHeader(string no_so)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXECUTE igd_UpdateCetakStsApSOHeader @No_SO = '" + no_so.Replace("@", "/") + "'";
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        // api/ResepNonRacik/UpdateUlangApSOHeader
        [HttpPut("UpdateUlangApSOHeader/{no_so}")]
        public IActionResult UpdateUlangApSOHeader(string no_so)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXECUTE igd_UpdateCetakUlangStsApSOHeader @No_SO = '" + no_so.Replace("@", "/") + "'";
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        // Start Riwayat Resep Non Racik
        // api/ResepNonRacik/GetAllRiwayatResepNRacik
        [HttpGet("GetAllRiwayatResepNRacik/{noreg}")]
        public IActionResult GetAllRiwayatResepNRacik(string noreg)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXEC igd_GetAllResepObatNRacik @NoReg='" + noreg + "'";
                // Console.WriteLine(query);
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONArrayString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JArray.Parse(res));
        }
    }
}