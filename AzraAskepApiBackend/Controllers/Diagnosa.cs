using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using AzraAskepApiBackend.Models;
using Newtonsoft.Json.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using AzraAskepApiBackend.Code;

namespace AzraAskepApiBackend.Controllers
{
	[Produces("application/json")]
    [Route("api/Diagnosa")]
    public class DiagnosaController : Controller
    {
    	private readonly ASKEPContext _context;
        DbSql oDb = new DbSql("Asskep", true);

        StringBuilder sb = new StringBuilder();
        DataSet ds = new DataSet();
        SqlCommand sqlCommand = new SqlCommand();

        StringBuilder sb1 = new StringBuilder();
        DataSet ds1 = new DataSet();
        SqlCommand sqlCommand1 = new SqlCommand();

        String result;

        public DiagnosaController(ASKEPContext context)
        {
            _context = context;
        }

        // api awal utk aplikasi dokter igd
        public static DataSet EscapeSpecialChar(DataSet ds)
        {
            int n;
            int x;
            int y;

            for (n = 0; n < ds.Tables.Count; n++)
            {
                DataTable temp = ds.Tables[n];
                for (x = 0; x < temp.Rows.Count; x++)
                {
                    DataRow dr = temp.Rows[x];
                    for (y = 0; y < temp.Columns.Count; y++)
                    {
                        DataColumn dc = temp.Columns[y];

                        if (dr[y].ToString().Contains(@"\"))
                        {
                            dr[y] = dr[y].ToString().Replace(@"\", @"\\");
                        }

                        if (dr[y].ToString().Contains("\""))
                        {
                            dr[y] = dr[y].ToString().Replace("\"", "\\\"");
                        }

                        if (dr[y].ToString().Contains("\\\\"))
                        {
                            dr[y] = dr[y].ToString().Replace("\\\\", "\\\\\\\\");
                        }

                        if (dr[y].ToString().Contains("\\\\\""))
                        {
                            dr[y] = dr[y].ToString().Replace("\\\\\"", "\\\"");
                        }

                        if (dr[y].ToString().Contains("\t"))
                        {
                            dr[y] = dr[y].ToString().Replace("\t", "\\t");
                        }

                        if (dr[y].ToString().Contains("\n"))
                        {
                            dr[y] = dr[y].ToString().Replace("\n", "\\n");
                        }

                        if (dr[y].ToString().Contains("\r"))
                        {
                            dr[y] = dr[y].ToString().Replace("\r", "\\r");
                        }

                        if (dr[y].ToString().Contains("\b"))
                        {
                            dr[y] = dr[y].ToString().Replace("\b", "\\b");
                        }

                        if (dr[y].ToString().Contains("\f"))
                        {
                            dr[y] = dr[y].ToString().Replace("\f", "\\f");
                        }

                        if (dr[y].ToString().Contains("/"))
                        {
                            dr[y] = dr[y].ToString().Replace("/", "\\/");
                        }
                    }
                }
            }

            return ds;
        }

        // note: api get diagnosa untuk pasien
        // api/Diagnosa/GetDiagnosaPasien
        [HttpGet("GetDiagnosaPasien/{deskterjemah}")]
        public IActionResult GetDiagnosaPasien(string deskterjemah)
        {
            string query = "";
            string res = "";

            try
            {
                query = "EXECUTE igd_GetDiagnosaPasein @DESCRIPTION = '"+ deskterjemah + "', @TERJEMAHAN = '"+ deskterjemah + "'";
                // Console.WriteLine(query);
                sb = new StringBuilder();
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);

                if (ds.Tables[0].Rows.Count != 0)
                {
                    //calling method for escape special char before they convert to JSON (available on the top of MainOrganization Controller)
                    ds = DiagnosaController.EscapeSpecialChar(ds);
                }

                res = DbSql.GetJSONArrayString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JArray.Parse(res));
        }

        // Start Riwayat Diagnosa
        // api/Diagnosa/GetAllDiagnosaPasien
        [HttpGet("GetAllDiagnosaPasien/{kdrm}")]
        public IActionResult GetAllDiagnosaPasien(string kdrm)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXEC igd_GetAllRiwayatDiagnosaPasien @Kd_RM='" + kdrm + "'";
                // Console.WriteLine(query);
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONArrayString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JArray.Parse(res));
        }

        // api/Diagnosa/GetDetailDiagnosaPasien
        [HttpGet("GetDetailDiagnosaPasien/{kdrm}/{noreg}")]
        public IActionResult GetDetailDiagnosaPasien(string kdrm, string noreg)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXEC igd_GetDetailRiwayatDiagnosaPasien @Kd_RM='" + kdrm + "', @NoReg='" + noreg + "'";
                // Console.WriteLine(query);
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        // api/Diagnosa/GetSkrinningPasien
        [HttpGet("GetSkrinningPasien/{noreg}")]
        public IActionResult GetSkrinningPasien(string noreg)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXEC igd_GetSkrinningPasienRJ @NoReg='" + noreg + "'";
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        // Start Riwayat Resep Non Racik
        // api/Diagnosa/GetAllRiwayatResepNRacik
        [HttpGet("GetAllRiwayatResepNRacik/{noreg}")]
        public IActionResult GetAllRiwayatResepNRacik(string noreg)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXEC igd_GetAllResepObatNRacik @NoReg='" + noreg + "'";
                // Console.WriteLine(query);
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONArrayString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JArray.Parse(res));
        }

        // Start Riwayat Resep Racik
        // api/Diagnosa/GetAllRiwayatResepRacik
        [HttpGet("GetAllRiwayatResepRacik/{noreg}")]
        public IActionResult GetAllRiwayatResepRacik(string noreg)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXEC igd_GetAllResepObatRacik @NoReg='" + noreg + "'";
                // Console.WriteLine(query);
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONArrayString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JArray.Parse(res));
        }

        // Insert RJ Diagnosa
        // api/Diagnosa/InsertRDDiagnosa
        [HttpPost("InsertRDDiagnosa")]
        public IActionResult InsertRDDiagnosa([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXECUTE igd_InsertRD_Diagnosa @RD_NoReg = '" + data.RD_NoReg + "', @Kd_Dokter = '" + data.Kd_Dokter + "'";
                // Console.WriteLine(query);
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        // api/Diagnosa/UpdateRJDiagnosa
        [HttpPut("UpdateRJDiagnosa")]
        public IActionResult UpdateRJDiagnosa([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                string count = "SELECT COUNT(*) C FROM sirs..icdx WHERE [DESCRIPTION] + ' | ' + [TERJEMAHAN] = '" + data.Ket_Tindakan + "'";
                // Console.WriteLine(count);
                sb.AppendFormat(count);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                string cek = DbSql.GetJSONObjectString(ds.Tables[0]);
                // Console.WriteLine(cek);
                if (JObject.Parse(cek)["C"].ToString() == "0")
                {
                    query = "EXECUTE igd_UpdateRD_Diagnosa @Kd_ICD = '', @Ket_ICD = '" + data.Ket_Tindakan + "', @UserID = '" + data.UserID + "', @RD_NoReg = '" + data.RD_NoReg + "'";
                }
                else
                {
                    query = "EXECUTE igd_UpdateRD_Diagnosa @Kd_ICD = '" + data.Kd_ICD + "', @Ket_ICD = '" + data.Ket_ICD + "', @UserID = '" + data.UserID + "', @RD_NoReg = '" + data.RD_NoReg + "'";
                }

                sb = new StringBuilder();
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        // api/Diagnosa/UpdateRDDiagnosa
        [HttpPut("UpdateRDDiagnosa")]
        public IActionResult UpdateRDDiagnosa([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXECUTE igd_UpdateRD_Diagnosa @Kd_ICD = '" + data.Kd_ICD + "', @Ket_ICD = '" + data.Ket_ICD + "', @RD_NoReg = '" + data.RD_NoReg + "'";
                Console.WriteLine(query);
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        // api/Diagnosa/InsertDiagnosaDetail
        [HttpPost("InsertDiagnosaDetail")]
        public IActionResult InsertDiagnosaDetail([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                for (int i = 0; i < data.data.Count; i++)
                {
                    sb = new StringBuilder();
                    query = "EXECUTE igd_InsertDiagnosaDetail @RD_NoReg = '" + data.RD_NoReg + "', @UserID = '" + data.UserID + "', " + 
                    "@Kd_ICD='" + data.data[i].ICD_CODE + "', @English_ICD='" + data.data[i].TERJEMAHAN + "'";
                     // Console.WriteLine(query);
                     sb.AppendFormat(query);
                     sqlCommand = new SqlCommand(sb.ToString());
                     ds = oDb.getDataSetFromSP(sqlCommand);
                     res = DbSql.GetJSONObjectString(ds.Tables[0]);
                }
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        // api/Diagnosa/UpdatePemeriksaan
        [HttpPut("UpdatePemeriksaanFisik")]
        public IActionResult UpdatePemeriksaanFisik([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXECUTE igd_UpdatePemeriksaanFisik @PemeriksaanFisik = '" + data.PemeriksaanFisik + "', @Planning = '" + data.Planning + "', @RD_NoReg = '" + data.RD_NoReg + "'";
                // Console.WriteLine(query);
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        // api/Diagnosa/UpdateSkrinningPasien
        [HttpPut("UpdateSkrinningPasien")]
        public IActionResult UpdateSkrinningPasien([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXECUTE igd_UpdateSkrinningPasien @KeluhanUtama = '" + data.KeluhanUtama + "', @NoReg = '" + data.NoReg + "'";
                // Console.WriteLine(query);
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        // api/Diagnosa/GetDiagnosaPasienByNoReg
        [HttpGet("GetDiagnosaPasienByNoReg/{NoReg}")]
        public IActionResult GetDiagnosaPasienByNoReg(string NoReg)
        {
            string query = "";
            string res = "";

            try
            {
                query = "EXECUTE igd_DiagnosaPasienDetail @RD_NoReg = '" + NoReg + "'";
                // Console.WriteLine(query);
                sb = new StringBuilder();
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONObjectString(ds.Tables[0]);
                // Console.WriteLine(res);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }       

        // api/Diagnosa/GetDiagnosaDetailByNoReg
        [HttpGet("GetDiagnosaDetailByNoReg/{NoReg}")]
        public IActionResult GetDiagnosaDetailByNoReg(string NoReg)
        {
            string query = "";
            string res = "";

            try
            {
                query = "EXECUTE igd_DiagnosaDetail @RD_NoReg = '" + NoReg + "'";
                // Console.WriteLine(query);
                sb = new StringBuilder();
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONArrayString(ds.Tables[0]);
                // Console.WriteLine(res);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JArray.Parse(res));
        }

        [HttpGet("GetTTVByNoReg/{NoReg}")]
        public IActionResult GetTTVByNoReg(string NoReg)
        {
            string query = "";
            string res = "";

            try
            {
                query = "EXECUTE sp_DiagnosaTTV @NoReg = '" + NoReg + "'";
                sb = new StringBuilder();
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONArrayString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"" + sb.ToString() + "}";
            }

            return Ok(JArray.Parse(res));
        }
    }
}