using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using AzraAskepApiBackend.Models;
using Newtonsoft.Json.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using AzraAskepApiBackend.Code;

namespace AzraAskepApiBackend.Controllers
{
	[Produces("application/json")]
    [Route("api/ResepRacik")]
    public class ResepRacikController : Controller
    {	
    	private readonly ASKEPContext _context;
        DbSql oDb = new DbSql("Asskep", true);

        StringBuilder sb = new StringBuilder();
        DataSet ds = new DataSet();
        SqlCommand sqlCommand = new SqlCommand();

        StringBuilder sb1 = new StringBuilder();
        DataSet ds1 = new DataSet();
        SqlCommand sqlCommand1 = new SqlCommand();

        String result;

        public ResepRacikController(ASKEPContext context)
        {
            _context = context;
        }

        // api awal utk aplikasi dokter igd
        public static DataSet EscapeSpecialChar(DataSet ds)
        {
            int n;
            int x;
            int y;

            for (n = 0; n < ds.Tables.Count; n++)
            {
                DataTable temp = ds.Tables[n];
                for (x = 0; x < temp.Rows.Count; x++)
                {
                    DataRow dr = temp.Rows[x];
                    for (y = 0; y < temp.Columns.Count; y++)
                    {
                        DataColumn dc = temp.Columns[y];

                        if (dr[y].ToString().Contains(@"\"))
                        {
                            dr[y] = dr[y].ToString().Replace(@"\", @"\\");
                        }

                        if (dr[y].ToString().Contains("\""))
                        {
                            dr[y] = dr[y].ToString().Replace("\"", "\\\"");
                        }

                        if (dr[y].ToString().Contains("\\\\"))
                        {
                            dr[y] = dr[y].ToString().Replace("\\\\", "\\\\\\\\");
                        }

                        if (dr[y].ToString().Contains("\\\\\""))
                        {
                            dr[y] = dr[y].ToString().Replace("\\\\\"", "\\\"");
                        }

                        if (dr[y].ToString().Contains("\t"))
                        {
                            dr[y] = dr[y].ToString().Replace("\t", "\\t");
                        }

                        if (dr[y].ToString().Contains("\n"))
                        {
                            dr[y] = dr[y].ToString().Replace("\n", "\\n");
                        }

                        if (dr[y].ToString().Contains("\r"))
                        {
                            dr[y] = dr[y].ToString().Replace("\r", "\\r");
                        }

                        if (dr[y].ToString().Contains("\b"))
                        {
                            dr[y] = dr[y].ToString().Replace("\b", "\\b");
                        }

                        if (dr[y].ToString().Contains("\f"))
                        {
                            dr[y] = dr[y].ToString().Replace("\f", "\\f");
                        }

                        if (dr[y].ToString().Contains("/"))
                        {
                            dr[y] = dr[y].ToString().Replace("/", "\\/");
                        }
                    }
                }
            }
            return ds;
        }

        // api/ResepRacik/CekNoSOApSORacikH
        [HttpGet("CekNoSOApSORacikH/{no_so}")]
        public IActionResult CekNoSOApSORacikH(string no_so)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXEC igd_CekNoSOApSORacik @No_SO = '" + no_so.Replace("@","/") + "'";
                // Console.WriteLine(query);
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        // note: api get paket obat racik berdasarkan kode dokter
        // api/ResepRacik/GetPaketObatRacikByDok
        [HttpGet("GetPaketObatRacikByDok/{kd_dokter}")]
        public IActionResult GetPaketObatRacikByDok(string kd_dokter)
        {
            string query = "";
            string res = "";

            try
            {
                query = "EXEC igd_GetPaketObatRacik @Kd_Dokter = '" + kd_dokter + "'";
                // Console.WriteLine(query);
                sb = new StringBuilder();
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONArrayString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }


            return Ok(JArray.Parse(res));
        }

        // note: api menampilkan list obat berdasrkan paket obat yg dipilih pada paket obat non racik
        // api/ResepRacik/GetObatByPaketObatRacik
        [HttpGet("GetObatByPaketObatRacik/{kd_dokter}/{kd_paket}")]
        public IActionResult GetObatByPaketObatRacik(string kd_dokter, string kd_paket)
        {
            string query = "";
            string res = "";

            try
            {
                query = "EXEC igd_GetPaketObatRacikDetail @Kd_Dokter = '"+ kd_dokter +"', @Kd_Template = '"+ kd_paket +"'";
                // Console.WriteLine(query);
                sb = new StringBuilder();
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONArrayString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JArray.Parse(res));
        }

        // note: membuat paket obat baru pada paket obat racik
        // api/ResepRacik/PostApSOTemplate_Resep_Racik
        [HttpPost("PostApSOTemplate_Resep_Racik")]
        public IActionResult PostApSOTemplate_Resep_Racik([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXECUTE igd_Insert_ApSOTemplate_Resep_Racik @kd_dokter = '" + data.kd_dokter + "', @nm_template = '" + data.nm_template + "', " +
                    "@satuan = '" + data.satuan + "', @aturan_pakai = '" + data.aturan_pakai + "'";
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        // note: menambahkan obat pada paket obat berdasarkan kd_dokter pada paket obat racik
        // api/ResepRacik/PostApSOTemplate_Resep_Racik_dt
        [HttpPost("PostApSOTemplate_Resep_Racik_dt")]
        public IActionResult PostApSOTemplate_Resep_Racik_dt([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                for (int i = 0; i < data.data.Count; i++)
                {
                    sb = new StringBuilder();
                    query = "EXECUTE igd_Insert_ApSOTemplate_Resep_Racik_dt @kd_dokter = '" + data.kd_dokter + "', @kd_template = '" + data.kd_template + "', " +
                    "@jumlah = '" + data.data[i].Jml_SO + "', @satuan = '" + data.data[i].Sat_Brg + "', @kd_barang = '" + data.data[i].Kd_Brg + "'";
                    sb.AppendFormat(query);
                    sqlCommand = new SqlCommand(sb.ToString());
                    ds = oDb.getDataSetFromSP(sqlCommand);
                    res = DbSql.GetJSONObjectString(ds.Tables[0]);
                }
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        // api/ResepRacik/GenerateRacikHeader
        [HttpPost("GenerateRacikHeader")]
        public IActionResult GenerateRacikHeader([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXECUTE igd_GenerateRacikHeader @no_so = '" + data.no_so + "'";
                // Console.WriteLine(query);
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        // post ke tabel ApSoRacikH
        // api/ResepRacik/PostApSoRacikH
        [HttpPost("PostApSoRacikH")]
        public IActionResult PostApSoRacikH([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXECUTE igd_InsertApSoRacikH @no_so = '" + data.no_so + "', @kd_racik = '" + data.kd_racik + "', " +
                    "@no_urut = '" + data.no_urut + "', @nama_racikan = '" + data.nama_racikan + "', @jml_ns = '" + data.jml_ns + "', @satuan_racik = '" + data.satuan_racik + "', @dosis_racik = '" + data.dosis_racik + "', @info_obat_racik = '" + data.info_obat_racik + "', @catatan_racik = '" + data.catatan_racik + "', @ket_racik = '" + data.ket_racik + "', @jml_rmw = '" + data.jml_rmw + "'";
                // Console.WriteLine(query);
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        // api/ResepRacik/PostApSORacikD
        [HttpPost("PostApSORacikD")]
        public IActionResult PostApSORacikD([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                // Console.WriteLine(data.data);
                for (int i = 0; i < data.dataRacik.Count; i++)
                {
                    sb = new StringBuilder();
                    query = "EXECUTE igd_InsertApSORacikD @no_so = '" + data.no_so + "', @kd_racik = '" + data.kd_racik + "', " +
                        "@no_urut = '" + data.no_urut + "', @kd_brg='" + data.dataRacik[i].Kd_Brg + "', @jumlah='" + data.dataRacik[i].Jml_SO + "', @sat_brg='" + data.dataRacik[i].Sat_SO + "'";
                    //  Console.WriteLine(query);
                     sb.AppendFormat(query);
                     sqlCommand = new SqlCommand(sb.ToString());
                     ds = oDb.getDataSetFromSP(sqlCommand);
                     res = DbSql.GetJSONObjectString(ds.Tables[0]);
                }
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        // api untuk menampilkan data racik obat yg sudah tersimpan di db ketika di halaman update resep
        // api/ResepRacik/GetResepObatRacik
        [HttpGet("GetResepObatRacik/{no_reg}")]
        public IActionResult GetResepObatRacik(string no_reg)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXEC igd_GetResepObatRacik @No_Reg='" + no_reg + "'";
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONArrayString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JArray.Parse(res));
        }

        // api untuk menampilkan resep racik yg baru tersimpan di form simpan
        // api/ResepRacik/GetResepRacikNew
        [HttpGet("GetResepRacikNew/{no_so}")]
        public IActionResult GetResepRacikNew(string no_so)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXEC igd_GetDataRacikNew @no_so='" + no_so.Replace("@","/") + "'";
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONArrayString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JArray.Parse(res));
        }

        // api untuk menampilkan detail obat racik
        // api/ResepRacik/GetResepObatRacikDetail
        [HttpGet("GetResepObatRacikDetail/{no_so}/{kd_racik}")]
        public IActionResult GetResepObatRacikDetail(string no_so, string kd_racik)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXEC igd_GetResepObatRacikDetail @no_so='" + no_so.Replace("@","/") + "', @kd_racik='" + kd_racik +"'";
                // Console.WriteLine(query);
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONArrayString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JArray.Parse(res));
        }

        // api/ResepRacik/DeleteApSORacikH
        [HttpPost("DeleteApSORacikH")]
        public IActionResult DeleteApSORacikH([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXECUTE igd_DeleteApSORacikH @no_so = '" + data.no_so + "', @kd_racik = '" + data.kd_racik + "'";
                // Console.WriteLine(query);
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        // api/ResepRacik/DeleteApSORacikD
        [HttpPost("DeleteApSORacikD")]
        public IActionResult DeleteApSORacikD([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXECUTE igd_DeleteApSORacikD @no_so = '" + data.no_so + "', @kd_racik = '" + data.kd_racik + "', " +
                    "@kd_brg = '" + data.kd_brg + "'";
                // Console.WriteLine(query);
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        // Start Riwayat Resep Racik
        // api/ResepRacik/GetAllRiwayatResepRacik
        [HttpGet("GetAllRiwayatResepRacik/{noreg}")]
        public IActionResult GetAllRiwayatResepRacik(string noreg)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXEC igd_GetAllResepObatRacik @NoReg='" + noreg + "'";
                // Console.WriteLine(query);
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONArrayString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JArray.Parse(res));
        }

        // api/ResepRacik/GetSatuanObat
        [HttpGet("GetSatuanObat")]
        public IActionResult GetSatuanObat()
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "SELECT * FROM MasterSatuan";
                // Console.WriteLine(query);
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);

                if (ds.Tables[0].Rows.Count != 0)
                {
                    ds = ResepRacikController.EscapeSpecialChar(ds);
                }
                
                res = DbSql.GetJSONArrayString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JArray.Parse(res));
        }
    }
}