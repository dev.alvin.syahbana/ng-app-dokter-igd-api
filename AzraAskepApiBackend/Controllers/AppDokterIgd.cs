using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using AzraAskepApiBackend.Models;
using Newtonsoft.Json.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using AzraAskepApiBackend.Code;

namespace AzraAskepApiBackend.Controllers
{
	[Produces("application/json")]
    [Route("api/AppDokterIgd")]
    public class AppDokterIgdController : Controller
    {
    	private readonly ASKEPContext _context;
        DbSql oDb = new DbSql("Asskep", true);

        StringBuilder sb = new StringBuilder();
        DataSet ds = new DataSet();
        SqlCommand sqlCommand = new SqlCommand();

        StringBuilder sb1 = new StringBuilder();
        DataSet ds1 = new DataSet();
        SqlCommand sqlCommand1 = new SqlCommand();

        String result;

        public AppDokterIgdController(ASKEPContext context)
        {
            _context = context;
        }

        // api awal utk aplikasi dokter igd
        public static DataSet EscapeSpecialChar(DataSet ds)
        {
            int n;
            int x;
            int y;

            for (n = 0; n < ds.Tables.Count; n++)
            {
                DataTable temp = ds.Tables[n];
                for (x = 0; x < temp.Rows.Count; x++)
                {
                    DataRow dr = temp.Rows[x];
                    for (y = 0; y < temp.Columns.Count; y++)
                    {
                        DataColumn dc = temp.Columns[y];

                        if (dr[y].ToString().Contains(@"\"))
                        {
                            dr[y] = dr[y].ToString().Replace(@"\", @"\\");
                        }

                        if (dr[y].ToString().Contains("\""))
                        {
                            dr[y] = dr[y].ToString().Replace("\"", "\\\"");
                        }

                        if (dr[y].ToString().Contains("\\\\"))
                        {
                            dr[y] = dr[y].ToString().Replace("\\\\", "\\\\\\\\");
                        }

                        if (dr[y].ToString().Contains("\\\\\""))
                        {
                            dr[y] = dr[y].ToString().Replace("\\\\\"", "\\\"");
                        }

                        if (dr[y].ToString().Contains("\t"))
                        {
                            dr[y] = dr[y].ToString().Replace("\t", "\\t");
                        }

                        if (dr[y].ToString().Contains("\n"))
                        {
                            dr[y] = dr[y].ToString().Replace("\n", "\\n");
                        }

                        if (dr[y].ToString().Contains("\r"))
                        {
                            dr[y] = dr[y].ToString().Replace("\r", "\\r");
                        }

                        if (dr[y].ToString().Contains("\b"))
                        {
                            dr[y] = dr[y].ToString().Replace("\b", "\\b");
                        }

                        if (dr[y].ToString().Contains("\f"))
                        {
                            dr[y] = dr[y].ToString().Replace("\f", "\\f");
                        }

                        if (dr[y].ToString().Contains("/"))
                        {
                            dr[y] = dr[y].ToString().Replace("/", "\\/");
                        }
                    }
                }
            }

            return ds;
        }

        // api/AppDokterIgd/GetUserIdDOkter
        [HttpGet("GetUserIdDOkter/{userid}/{pass}")]
        public IActionResult GetUserIdDOkter(string userid, string pass)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXEC igd_GetUserIdDokter @UserIDDokter='" + userid + "', @PassDokter='" + pass + "'";
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        // api/AppDokterIgd/ListPasienIGD
        [HttpPost("ListPasienIGD")]
        public IActionResult ListPasienIGD([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXECUTE igd_GetListPasienIgd @tanggal = '"+ data.tanggal + "', @kd_dokter = '" + data.kd_dokter+ "'";
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONArrayString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JArray.Parse(res));
        }

        // api/AppDokterIgd/GetDetailPasienIGD
        [HttpGet("GetDetailPasienIGD/{kd_rm}/{rd_noreg}")]
        public IActionResult GetDetailPasienIGD(string kd_rm, string rd_noreg)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXEC igd_GetDetailPasienIGD @Kd_RM = '" + kd_rm + "',  @RD_NoReg = '" + rd_noreg + "'";
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        // Get Nomor Igd
        // api/AppDokterIgd/GetNoIgd
        [HttpGet("GetNoIgd")]
        public IActionResult GetNoIgd()
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXEC igd_GetNoIgd";
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        // Update Status Panggil 
        // api/AppDokterIgd/UpdStsPanggil
        [HttpPost("UpdStsPanggil")]
        public IActionResult UpdStsPanggil([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXECUTE SIRSINFO.dbo.updstatusPanggil @Ticket_ID = '" + data.ticket_id + "', @Ticket_Posisi = '" + data.ticket_posisi + "'";
                // Console.WriteLine(query);
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        // api/AppDokterIgd/UpdateALergiApSOHeader
        [HttpPut("UpdateALergiApSOHeader/{no_reg}")]
        public IActionResult UpdateALergiApSOHeader([FromBody] dynamic data, string no_reg)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXECUTE igd_UpdateAlergiApSOHeader @Alergi = '" + data.alergi + "',  @NoReg = '" + no_reg + "'";
                // Console.WriteLine(query);
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        // api/AppDokterIgd/UpdateALergidbPasienRS
        [HttpPut("UpdateALergidbPasienRS/{kd_rm}")]
        public IActionResult UpdateALergidbPasienRS([FromBody] dynamic data, string kd_rm)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXECUTE igd_UpdateAlergidbPasienRS @Alergi = '" + data.alergi + "',  @Kd_RM = '" + kd_rm + "'";
                // Console.WriteLine(query);
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        // api/AppDokterIgd/GetTrxPasienRM
        [HttpPost("GetTrxPasienRM")]
        public IActionResult GetTrxPasienRM([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXECUTE sp_GetTrxByRM @Kd_RM = '" + data.Kd_RM + "'";
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                // res = "{\"code\":\"1\",\"msg\":\"Success\"}";
                res = DbSql.GetJSONArrayString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"}";
                // res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JArray.Parse(res));
        }
        
        // api/AppDokterIgd/GetHasilLabByTrx
        [HttpPost("GetHasilLabByTrx")]
        public IActionResult GetHasilLabByTrx([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXECUTE IMAN..RptHasilLaboratorium @No_Trx = '" + data.No_Trx + "'";
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                // res = "{\"code\":\"1\",\"msg\":\"Success\"}";
                res = DbSql.GetJSONArrayString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"}";
                // res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JArray.Parse(res));
        }

        [HttpGet("GetJenisPelayananPM")]
        public IActionResult GetJenisPelayananPM()
        {
            string query = "";
            string res = "";

            try
            {
                query = "EXEC sp_GetJenisPelayanan";
                // Console.WriteLine(query);
                sb = new StringBuilder();
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONArrayString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }


            return Ok(JArray.Parse(res));
        }

        [HttpPost("GetPenunMedisByNoreg")]
        public IActionResult GetPenunMedisByNoreg([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                query = "EXECUTE sp_GetPelPenunjangMedis @Kd_Dokter = '" + data.Kd_Dokter + "', @No_Reg = '" + data.No_Reg + "'";
                sb = new StringBuilder();
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                // res = DbSql.GetJSONArrayString(ds.Tables[0]);
                res = DbSql.GetJSONArrayString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"}";
                // res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }
            return Ok(JArray.Parse(res));
        }

        [HttpPost("GetPemeriksaanByKdLyn")]
        public IActionResult GetPemeriksaanByKdLyn([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                query = "EXECUTE sp_GetPemeriksaanByKodeLyn @Kd_Lyn = '" + data.Kd_Lyn + "', @Kd_Tarif = '" + data.Kd_Tarif + "', " +
                    "@Kd_Dokter = '" + data.Kd_Dokter + "'";
                // Console.WriteLine(query);
                sb = new StringBuilder();
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONArrayString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JArray.Parse(res));
        }

        [HttpPost("GetEditTindMedis")]
        public IActionResult GetEditTindMedis([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXECUTE IMAN..sp_GetTindakanPenunjangMedis @No_Trx = '" + data.No_Trx + "', @Kd_Dokter = '" + data.Kd_Dokter + "'";
                // Console.WriteLine(query);
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONArrayString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"}";
                // res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JArray.Parse(res));
        }

        [HttpPost("DelPenunMedis")]
        public IActionResult DelPenunMedis([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXECUTE IMAN..DelPelayananPenunjangMedis @No_Trx = '" + data.No_Trx + "', @No_Reg = '" + data.No_Reg + "'";
                // Console.WriteLine(query);
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = "{\"code\":\"1\",\"msg\":\"Success\"}";
                // res = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"}";
                // res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        [HttpGet("GetDataTindakanPasien/{noreg}/{kdlyn}")]
        public IActionResult GetDataTindakanPasien(string noreg, string kdlyn)
        {
            string query = "";
            string res = "";

            try
            {
                query = "EXECUTE sp_GetDataTindakan @NoReg = '" + noreg + "', @Kd_Lyn = '" + kdlyn + "'";
                sb = new StringBuilder();
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONArrayString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JArray.Parse(res));
        }

        [HttpGet("GetObjKdKelas/{noreg}")]
        public IActionResult GetObjKdKelas(string noreg)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXEC IMAN..SeekRegistrasiForPM @NoReg='" + noreg + "'";
                // Console.WriteLine(query);
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONArrayString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JArray.Parse(res));
        }

        [HttpGet("GetPaketPMByDok/{kddok}/{kdlyn}")]
        public IActionResult GetPaketPMByDok(string kddok, string kdlyn)
        {
            string query = "";
            string res = "";

            try
            {
                query = "EXECUTE sp_GetPaketPM @Kd_Dokter = '" + kddok + "', @Kd_Lyn = '" + kdlyn + "'";
                sb = new StringBuilder();
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONArrayString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"}";
                // res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }


            return Ok(JArray.Parse(res));
        }

        [HttpPost("GetObjPemeriksaanByKdTndk")]
        public IActionResult GetObjPemeriksaanByKdTndk([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                query = "EXECUTE sp_GetObjPemeriksaanByKdTndk @Kd_Lyn = '" + data.Kd_Lyn + "', @Kd_Tarif = '" + data.Kd_Tarif + "', " +
                    "@Kd_Dokter = '" + data.Kd_Dokter + "', @Kd_Tindakan = '" + data.Kd_Tindakan + "'";
                // Console.WriteLine(query);
                sb = new StringBuilder();
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        [HttpPost("DelTindakanPenunMedis")]
        public IActionResult DelTindakanPenunMedis([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXECUTE IMAN..DelTindakanPelayananPenunjangMedis @No_Trx = '" + data.No_Trx + "', @Kd_Tindakan = '" + data.Kd_Tindakan + "'";
                // Console.WriteLine(query);
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = "{\"code\":\"1\",\"msg\":\"Success\"}";
                // res = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"}";
                // res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        [HttpGet("GetPemeriksaanByPaketPM/{kddok}/{kdtemp}/{kdplyn}")]
        public IActionResult GetPemeriksaanByPaketPM(string kddok, string kdtemp, string kdplyn)
        {
            string query = "";
            string res = "";

            try
            {
                query = "EXECUTE sp_GetPaketPMDetail @Kd_Dokter = '" + kddok + "', @Kd_Template = '" + kdtemp + "', @Kd_Lyn = '" + kdplyn + "'";
                sb = new StringBuilder();
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONArrayString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"}";
                // res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }


            return Ok(JArray.Parse(res));
        }

        [HttpPost("DelPaketPM")]
        public IActionResult DelPaketPM([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXECUTE sp_Delete_PaketPenunjangMedis @kd_dokter = '" + data.kd_dokter + "', @kd_template = '" + data.kd_template + "', @kd_lyn = '" + data.kd_lyn + "'";
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = "{\"code\":\"1\",\"msg\":\"Success\"}";
                // res = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"}";
                // res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        [HttpPost("InstPenunMedisH")]
        public IActionResult InstPenunMedisH([FromBody] dynamic data)
        {
            string query = "";
            string res = "";
            try
            {
                sb = new StringBuilder();
                query = "EXECUTE IMAN..NewInsTrxLaboratorium11 @No_Trx = '" + data[0].No_Trx + "', @Kd_Lyn = '" + data[0].Kd_Lyn + "', @No_Reg = '" + data[0].No_Reg + "', @No_Urut = '" + data[0].No_Urut + "', @Tgl_Periksa = '" + data[0].Tgl_Periksa + "', @Kd_Dokter_Kirim = '" + data[0].Kd_Dokter_Kirim + "', @Dokter_Kirim = '" + data[0].Dokter_Kirim + "', @Kd_DrLab = '" + data[0].Kd_DrLab + "', @Kd_PenataLab = '" + data[0].Kd_PenataLab + "',  @Jumlah = '" + data[0].TotalH + "', @Discount = '" + data[0].Discount + "', @Karyawan = '" + data[0].StatKaryawan + "', @FOC = '" + data[0].FOC + "', @UserID = '" + data[0].UserID + "', @KD_Kelas = '" + data[0].KD_Kelas + "', @Kunjungan = '" + data[0].Kunjungan + "', @Total = '" + data[0].TotalH + "'";

                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                // res = "{\"code\":\"1\",\"msg\":\"Success\"}";
                res = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"}";
                // res = "{\"code\":\"0\",\"msg\":\""+ sb.ToString() +"\"}";
            }
            return Ok(JObject.Parse(res));
        }

        [HttpPost("InstPenunMedisD")]
        public IActionResult InstPenunMedisD([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                for (int i = 0; i < data.Count; i++)
                {
                    sb = new StringBuilder();
                    query = "EXECUTE IMAN..NewSaveTrxLaboratorium_DT2 @No_Trx = '" + data[i].No_Trx + "', @Kd_Lyn = '" + data[i].Kd_Lyn + "', @Kd_Tindakan = '" + data[i].Kd_Tindakan + "', @CITO = '" + data[i].StatCITO + "', @Harga = '" + data[i].Harga + "', @Discount = '" + data[i].Discount + "', @UserID = '" + data[i].UserID + "', @jmlTrx = '" + data[i].JmlTrx + "'";
                    // Console.WriteLine(query);
                    sb.AppendFormat(query);
                    sqlCommand = new SqlCommand(sb.ToString());
                    ds = oDb.getDataSetFromSP(sqlCommand);
                    res = "{\"code\":\"1\",\"msg\":\"Success\"}";
                    // res = DbSql.GetJSONObjectString(ds.Tables[0]);
                }
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"}";
                // res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        [HttpPost("UpdtPenunMedisH")]
        public IActionResult UpdtPenunMedisH([FromBody] dynamic data)
        {
            string query = "";
            string res = "";
            try
            {
                sb = new StringBuilder();
                query = "EXECUTE IMAN..UpdtTrxLaboratorium11 @No_Trx = '" + data[0].No_Trx + "', @Kd_Lyn = '" + data[0].Kd_Lyn + "', @No_Reg = '" + data[0].No_Reg + "', @No_Urut = '" + data[0].No_Urut + "', @Tgl_Periksa = '" + data[0].Tgl_Periksa + "', @Kd_Dokter_Kirim = '" + data[0].Kd_Dokter_Kirim + "', @Dokter_Kirim = '" + data[0].Dokter_Kirim + "', @Kd_DrLab = '" + data[0].Kd_DrLab + "', @Kd_PenataLab = '" + data[0].Kd_PenataLab + "',  @Jumlah = '" + data[0].TotalH + "', @Discount = '" + data[0].Discount + "', @Karyawan = '" + data[0].StatKaryawan + "', @FOC = '" + data[0].FOC + "', @UserID = '" + data[0].UserID + "', @KD_Kelas = '" + data[0].KD_Kelas + "', @Kunjungan = '" + data[0].Kunjungan + "', @Total = '" + data[0].TotalH + "'";

                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = "{\"code\":\"1\",\"msg\":\"Success\"}";
                // res = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"}";
                // res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }
            return Ok(JObject.Parse(res));
        }

        [HttpPost("UpdtPenunMedisD")]
        public IActionResult UpdtPenunMedisD([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                for (int i = 0; i < data.Count; i++)
                {
                    sb = new StringBuilder();
                    query = "EXECUTE IMAN..UpdtTrxLaboratorium_DT2 @No_Trx = '" + data[i].No_Trx + "', @Kd_Lyn = '" + data[i].Kd_Lyn + "', @Kd_Tindakan = '" + data[i].Kd_Tindakan + "', @CITO = '" + data[i].StatCITO + "', @Harga = '" + data[i].Harga + "', @Discount = '" + data[i].Discount + "', @UserID = '" + data[i].UserID + "', @jmlTrx = '" + data[i].JmlTrx + "'";
                    // Console.WriteLine(query);
                    sb.AppendFormat(query);
                    sqlCommand = new SqlCommand(sb.ToString());
                    ds = oDb.getDataSetFromSP(sqlCommand);
                    res = "{\"code\":\"1\",\"msg\":\"Success\"}";
                    // res = DbSql.GetJSONObjectString(ds.Tables[0]);
                }
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"}";
                // res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        [HttpPost("InstPaketPM")]
        public IActionResult InstPaketPM([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXECUTE sp_Insert_PaketPenunjangMedis @kd_dokter = '" + data.kd_dokter + "', @nm_template = '" + data.nm_template + "', @kd_poli = '" + data.kd_poli + "', @kd_lyn = '" + data.kd_lyn + "'";
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = "{\"code\":\"1\",\"msg\":\"Success\"}";
                // res = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"}";
                // res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        [HttpPost("InstPaketPMDT")]
        public IActionResult InstPaketPMDT([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                for (int i = 0; i < data.data.Count; i++)
                {
                    sb = new StringBuilder();
                    query = "EXECUTE sp_Insert_PaketPenunjangMedis_dt @kd_dokter = '" + data.kd_dokter + "', @kd_tindakan = '" + data.data[i].Kd_Tindakan + "', " +
                    "@jml_so = '" + data.data[i].JmlTrx + "', @kd_poli = '" + data.kd_poli + "', @kd_lyn = '" + data.data[i].Kd_Lyn + "'";
                    sb.AppendFormat(query);
                    sqlCommand = new SqlCommand(sb.ToString());
                    ds = oDb.getDataSetFromSP(sqlCommand);
                    res = "{\"code\":\"1\",\"msg\":\"Success\"}";
                    // res = DbSql.GetJSONObjectString(ds.Tables[0]);

                }
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"}";
                // res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        [HttpPost("InsertLogSystem")]
        public IActionResult InsertLogSystem([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXECUTE SIRSINFO..sp_InstLogAppDokter @NoReg = '" + data.NoReg + "', @Kd_Dokter = '" + data.Kd_Dokter + "', @Aksi = '" + data.Aksi + "', @Jenis = '" + data.Jenis + "'";
                // Console.WriteLine(query);
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = "{\"code\":\"1\",\"msg\":\"Success\"}";
                // res = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"}";
            }

            return Ok(JObject.Parse(res));
        }

        [HttpGet("GetTindakanJPRS/{kddokter}/{kdlyn}")]
        public IActionResult GetTindakanJPRS(string kddokter, string kdlyn)
        {
            string query = "";
            string res = "";

            try
            {
                query = "EXECUTE sp_GetTindakanJPRS @Kd_Dokter = '" + kddokter + "', @Kd_Lyn = '" + kdlyn + "'";
                sb = new StringBuilder();
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);

                if (ds.Tables[0].Rows.Count != 0)
                {
                    ds = AppDokterIgdController.EscapeSpecialChar(ds);
                }

                res = DbSql.GetJSONArrayString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JArray.Parse(res));
        }

        [HttpPost("DeleteListTindakan")]
        public IActionResult DeleteListTindakan([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                sb = new StringBuilder();
                query = "EXECUTE sp_DeleteListTindakanPasien @NoReg = '" + data.noreg + "', @Kd_Lyn = '" + data.kdlyn + "', @Kd_Tindakan = '" + data.kdtind + "'";
                sb.AppendFormat(query);
                sqlCommand = new SqlCommand(sb.ToString());
                ds = oDb.getDataSetFromSP(sqlCommand);
                res = DbSql.GetJSONObjectString(ds.Tables[0]);
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }

        [HttpPost("PostTindakan")]
        public IActionResult PostTindakan([FromBody] dynamic data)
        {
            string query = "";
            string res = "";

            try
            {
                for (int i = 0; i < data.data.Count; i++)
                {
                    sb = new StringBuilder();
                    query = "EXECUTE InstmpTrxRJ_Tindakan @NoReg = '" + data.NoReg + "', @Kd_Lyn = '" + data.Kd_Lyn + "', @Kd_Tindakan = '" + data.data[i].Kd_Tindakan + "', @CITO = '" + data.data[i].Cito + "', @QTY = '" + data.data[i].Qty + "', @Harga = '" + data.data[i].Harga + "', @Disc = '" + data.data[i].Disc + "', @Total = '" + data.data[i].Total + "', @HrgNormal = '" + data.data[i].Hrg_Std + "', @HrgCITO = '" + data.data[i].Hrg_CITO + "', @HrgKary = '" + data.data[i].Hrg_Kary + "'";                    
                    // Console.WriteLine(query);
                    sb.AppendFormat(query);
                    sqlCommand = new SqlCommand(sb.ToString());
                    ds = oDb.getDataSetFromSP(sqlCommand);
                    res = DbSql.GetJSONObjectString(ds.Tables[0]);
                }
            }
            catch
            {
                res = "{\"code\":\"0\",\"msg\":\"Gagal\"" + sb.ToString() + "}";
            }

            return Ok(JObject.Parse(res));
        }
    }
}