﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace AzraAskepApiBackend.Models
{
    public partial class ASKEPContext : DbContext
    {
        
        public ASKEPContext(DbContextOptions<ASKEPContext> options)
          : base(options)
        { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            
        }
    }
}
